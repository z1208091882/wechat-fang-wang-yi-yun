// pages/index/index.js
import request from "../../util/requset";
Page({
  /**
   * 页面的初始数据
   */
  data: {
    banner: [], //轮播图的数据
    recommendList: [], //推荐歌单的数据
    topList: [], //排行榜数据
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    //  引入封装的功能函数 轮播图数据
    const bannerList = await request("banner", {
      type: 2,
    });
    // console.log(bannerList,"页面的数据");
    // 推荐歌单数据
    const recommendList = await request("personalized", {
      limit: 10,
    });
    // console.log(recommendList);
    // 排行榜数据
    /*
    需求分析：
      1.需要根据idx的值获取对应数据
      2.idx的取值范围是0-20，我们需要0-4
      3.所以我们需要发送5ci请求
    前++和后++区别
      1.先看是运行符还是值
     2.先自增：++a： + 号在前， 表示先 + 1， 后返回值
     3.后自增： a++： + 号在后， 表示先返回值， 再 + 1
    */
    //  使用while循环
    // let a = 0
    // while (a < 5) {
    //   console.log(a);
    // }

    var resultArr = [];
    for (let index = 0; index < 5; index++) {
      // const element = array[index];
      // console.log(index)
      let topListData = await request("top/List", {
        idx: index,
      });
      // console.log(topListData);
      //因为这个对象返回的数据特别庞大 在这先拿出来自己用的就行
      let topListItem = {
        name: topListData.playlist.name,
        // 截取数组 splice(会修改原数组，可以对指定的数组进行增删改查) slice(不会修改原数组)
        track: topListData.playlist.tracks.slice(0, 3),
      };
      // console.log(topListItem);
      // // 每次循环完的都放在这个数组里边
      resultArr.push(topListItem);
      // console.log(resultArr);
      this.setData({
        topList: resultArr,
      });
    }

    this.setData({
      banner: bannerList.banners,
      recommendList: recommendList.result,
    });
  },
  //跳转到每日推荐界面
  toCommendSong() {
    console.log(333);
    wx.navigateTo({
      url: "/songPackage/pages/recommendSong/recommendSong",
    });
  },
  // 跳到other页面
  toOther() {
    console.log(444);
    wx.navigateTo({
      url: "/otherPackage/pages/other/other",
    });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {},

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {},

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {},

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {},

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {},
});
