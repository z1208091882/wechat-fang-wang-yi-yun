import request from "../../util/requset";
let isSend = null; //函数防抖使用
Page({
  /**
   * 页面的初始数据
   */
  data: {
    placeholderContent: "", //placeholderContent的默认内容
    hotList: [], //热搜榜的数据
    searchContent: "", //用户输入的数据
    searchList: [], //关键字模糊匹配的数据
    historyList: [], //搜索历史记录
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    // 调用 获取初始化的数据
    this.getInfoDate();
    this.getSearchHistory();
  },
  // 清空搜索内容回调函数
  clearSearchContent() {
    let { searchContent } = this.data;
    searchContent = "";
    historyList = [];
    this.setData({
      searchContent,
    });
  },
  // 删除历史记录的回调函数
  deleteSearchHistory() {
    // 消息提示框
    var that = this;
    wx.showModal({
      content: "是否确认清空历史记录",
      success(res) {
        if (res.confirm) {
          console.log("用户点击确定");
          // 在成功的回调函数中不可以直接修改状态  可以定义个变量=原来的this，然后在进行修改
          that.setData({
            historyList: [],
          });
        } else if (res.cancel) {
          console.log("用户点击取消");
        }
      },
    });
  },
  // 获取本地存储的历史记录
  getSearchHistory() {
    let historyList = wx.getStorageSync("searchHistory");
    this.setData({
      historyList,
    });
  },

  // 获取初始化的数据
  async getInfoDate() {
    //  获取placeholder初始化的数据
    const placeholderData = await request("search/default");
    //获取热搜榜的数据
    const hotListData = await request("search/hot/detail");
    console.log(hotListData);
    // console.log(placeholderData);
    // 更新状态
    this.setData({
      placeholderContent: placeholderData.data.showKeyword,
      hotList: hotListData.data,
    });
  },
  // 搜素内容的回调函数
  handleInputChange(event) {
    // 获取输入框内容的数据
    // console.log(event.detail.value);

    this.setData({
      // 将输入的内容放在状态里面并去掉空格
      searchContent: event.detail.value.trim(),
    });
    // 函数防抖
    if (isSend) {
      clearTimeout(isSend);
    }
    isSend = setTimeout(() => {
      // 发请求获取关键字模糊匹配的数据
      this.getSearchList();
    }, 300);
  },

  // 获取搜索数据的功能函数
  async getSearchList() {
    // 假如说表单的内容为空就直接return
    if (!this.data.searchContent) {
      //在输入框的结果为空时，将这个搜索的内容也设为空
      this.setData({
        searchList: [],
      });
      return;
    }

    let { searchContent, historyList } = this.data;
    // 发请求获取关键字模糊匹配的数据
    let searchListData = await request("search", {
      keywords: searchContent,
      limit: 10,
    });
    // console.log(searchListData);
    this.setData({
      searchList: searchListData.result.songs,
    });

    // 将搜索的关键字添加到搜素的历史记录区域
    //如果历史记录里边没有数据的时候就push进去
    if (historyList.indexOf(searchContent) !== -1) {
      // 如果有就进行删除 当前的小标
      historyList.splice(historyList.indexOf(searchContent), 1);
    }
    // 在重新push进行去
    historyList.unshift(searchContent);
    this.setData({
      historyList,
    });
    // 将数据存到本地 避免一刷新就没有了
    wx.setStorageSync("searchHistory", historyList);
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {},

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {},

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {},

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {},

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {},
});
