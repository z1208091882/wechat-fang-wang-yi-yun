import request from "../../util/requset"
let startY = 0; //手指点击起始的纵向坐标
let moveY = 0; //手指移动的纵向坐标
let moveDistance = 0 //手指移动的距离=手指移动的坐标-手指起始的坐标
Page({

  /**
   * 页面的初始数据
   */
  data: {
    coverTransform: "translateY(0)", //初始位移为0
    coveTransition: "", //过度效果
    userInfo: {}, //用户的信息
    recentplayLiust: [], //用户的播放记录
  },
  // 手指移动开始
  handleTouchStart(event) {
    // this.setData({
    //   coveTransition: "transform 0.1s "
    // })
    // 获取手指的起始坐标
    // 用event对象拿到第一个手指触摸的坐标[0].clientY纵向坐标
    startY = event.touches[0].clientY
    // console.log("start");
    this.setData({
      coveTransition: ""
    })

  },
  // 手指移动
  handleTouchMove(event) {
    // 获取手指的移动坐标
    moveY = event.touches[0].clientY
    // 获取手指移动的距离
    moveDistance = moveY - startY
    /* 
    往下走是正数  往上走是负数
     console.log(moveDistance);
     只让他往下走80rpx
     */
    if (moveDistance <= 0) {
      return
    }
    if (moveDistance >= 80) {
      moveDistance = 80
    }
    // 动态更新coverTransform里的状态值
    this.setData({
      coverTransform: `translateY(${moveDistance}px)`, //移动

    })
  },
  // 手指移动结束
  handleTouchEnd() {
    this.setData({
      coverTransform: `translateY(0px)`,
      coveTransition: "transform 0.5s linear" //过度效果
    })
  },
  // 点击头像后的回调函数 跳转到login页面
  toLogin() {
    wx.navigateTo({
      url: '/pages/login/login',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 获取本地存储的数据同步模式
    let userInfo = wx.getStorageSync("userInfo")
    // console.log(userInfo);
    // 只有登陆了才进行显示用户的信息
    if (userInfo) { //用户登陆
      this.setData({
        userInfo: JSON.parse(userInfo)
      })
      // 获取用户的播放激励
      this.getUserRecentPlayList(this.data.userInfo.userId)
    }
  },
  // 获取用户记录的功能函数
  // 获取用户播放记录的功能函数
  async getUserRecentPlayList(userId) {
    let recentPlayListData = await request('user/record', {
      uid: userId,
      type: 0
    });
    console.log(recentPlayListData);

    let recentPlayList = recentPlayListData.allData.splice(0, 10)
    this.setData({
      recentPlayList
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})