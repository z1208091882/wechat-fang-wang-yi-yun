import requset from "../../util/requset"
/**
  作者: Created by zhiyongzaixian
  说明: 登录流程
  1. 收集表单项数据
  2. 前端验证
    1) 验证用户信息(账号，密码)是否合法
    2) 前端验证不通过就提示用户，不需要发请求给后端
    3) 前端验证通过了，发请求(携带账号, 密码)给服务器端
  3. 后端验证
    1) 验证用户是否存在
    2) 用户不存在直接返回，告诉前端用户不存在
    3) 用户存在需要验证密码是否正确
    4) 密码不正确返回给前端提示密码不正确
    5) 密码正确返回给前端数据，提示用户登录成功(会携带用户的相关信息)
*/
Page({

  /**
   * 页面的初始数据
   */
  data: {
    phone: "", //手机号
    password: "", //密码
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  // 获取输入框里的内容,获取手机号和密码输入的内容
  handleChange(event) {
    // console.log(event.detail.value);
    // 分别拿到当前的传过来的数据的id currentTarget是触发事件的本身 
    // let type = event.currentTarget.id //写法1 id取值： phone || possword  只需要传一个值可以使用id
    let type = event.currentTarget.dataset.type //写法2 使用自定义属性data-key=value 取值  穿多个数据使用data-type=value形式
    // console.log(event.currentTarget.dataset.type);
    this.setData({
      // 直接将这个得到的数据作为key名省去了在去遍历
      [type]: event.detail.value
    })
  },
  // 登陆前的问题
  async login() {
    // 1. 收集表单项数据
    let {
      phone,
      password
    } = this.data
    // --------------------------------------------------------------
    // 2.前端验证
    /*
    手机号验证
      1.内容为空
      2.手机号格式不正确
      3.手机号格式正正确，验证通过
    */
    if (!phone) {
      // 手机号码不能为空 提示用户
      wx.showToast({
        title: '手机号码不能为空',
        icon: "none" //不显示图标
      })
      return
    };
    // 定义正则表达式
    let phoneReg = /^1[3-9][0-9]{9}$/
    //通过正则的test方法(验证手机号)
    if (!phoneReg.test(phone)) {
      // 提示用户
      wx.showToast({
        title: '手机号码格式不正确',
        icon: "none" //不显示图标
      })
      return
    }
    // 密码的验证
    if (!password) {
      // 提示用户
      wx.showToast({
        title: '密码不能为空',
        icon: "none" //不显示图标
      })
      return
    }
    // 如果前边的验证全部通过 提示用户
    wx.showToast({
      title: '前端验证成功',
      icon: "success"
    })
    // ------------------------------------------------
    // 后端验证 发请求
    let result = await requset('login/cellphone', {
      phone,
      password,
      isLogin: true
    })
    console.log(result);
    if (result.code === 200) {
      //  提示用户
      wx.showToast({ //登陆成功
        title: '验证成功',
        icon: "success"
      })
      // 先将用户的信息存储本地 最好是json数据
      // 同步版本  异步的没有Sync
      // console.log(result);
      wx.setStorageSync('userInfo', JSON.stringify(result.profile))

      // console.log(222);
      // 跳转到个人中心personal文件
      wx.reLaunch({
        url: '/pages/personal/personal',
      })
    } else if (result.code === 400) {
      //  提示用户
      wx.showToast({
        title: '手机号错误',
        icon: "none"
      })
    } else if (result.code === 502) {
      //  提示用户
      wx.showToast({
        title: '密码错误',
        icon: "none"
      })
    } else { //最后都没有的情况下就是登陆失败
      //  提示用户
      wx.showToast({
        title: '登陆 失败，请重新登陆',
        icon: "none"
      })
    }

  },
  // --------------------------------------------------------



})