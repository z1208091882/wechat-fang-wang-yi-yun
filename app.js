App({
  /*
  问题：当用户在播放页面点击返回在此进入到音乐播放界面时，播放状态不匹配的问题
  因为：在点击返回时,这个组件的实例会被销毁，在此进入时会在此初始化，导致页面是播放状态不符
  解决方案：将这两个状态定义到全局的
  */
  globalData: {
    isMusicPlay: false, //是否有音乐在播放
    musicId: "", //标识音乐的Id
  },
  /**
   * 当小程序初始化完成时，会触发 onLaunch（全局只触发一次）
   */
  onLaunch: function () {},

  /**
   * 当小程序启动，或从后台进入前台显示，会触发 onShow
   */
  onShow: function (options) {},

  /**
   * 当小程序从前台进入后台，会触发 onHide
   */
  onHide: function () {},

  /**
   * 当小程序发生脚本错误，或者 api 调用失败时，会触发 onError 并带上错误信息
   */
  onError: function (msg) {},
});
