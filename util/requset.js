// 封装发送请求库 ajax
/*
1.封装功能函数如何封装
  1.功能点明确
  2.函数内部应该如何保留代码(静态的)
  3.将动态的数据抽取成形参，出使用者根据自身的情况动态的传入实参
  4.一个良好的功能函数应设置形参的默认值(es6的形参默认值)
2. 封装功能的组件  
   1.功能点明确
   2.函数内部应该如何保留代码(静态的)
   3.将动态的数据抽取成props参数，出使用者根据自身的情况一级标签属性的形式动态传入props数据
   4.一个良好的组件应设置组件的必要性以及数据类型
   props:{
     msg:{
       required:true,
       default:"默认值",
       type:String
     }
   }
*/
import config from "./config"; //引入ajax配置信息
export default (url, data = {}, method = "GET") => {
  // 1.new Promise初始化Promise的实例状态为pending
  return new Promise((resolve, reject) => {
    wx.request({
      url: config.host + url,
      data,
      method,
      header: {
        // 不能直接使用下标因为在此请求时回来的数据肯是乱的 先用indexOf(筛选一下，不等于-1的那个元素才返回包含这个字符串)
        // 有一种情况就是用户未登录 不应该有token 所以用一个3元 如果用户登陆了才进行显示，如果没登陆时不显示的
        cookie: wx.getStorageSync("cookie")
          ? wx
              .getStorageSync("cookie")
              .find((item) => item.indexOf("MUSIC_U") !== -1)
          : "",
      },

      // 请求的成功的数据
      success: (res) => {
        // console.log(res);
        //  在登陆的接口中多传入一个属性
        if (data.isLogin) {
          //在发送登陆请求后将得到的cookie存到本地
          // 将用户的tookie存到本地
          wx.setStorage({
            key: "cookie",
            data: res.cookies,
          });
        }
        // console.log(wx.getStorageSync("cookie"));
        resolve(res.data); //resolve修改promise的状态为成功的状态
      },
      //请求失败的数据
      fail: (err) => {
        // console.log("失败", err);
        reject(err); //reject修改Promise的状态为失败的状态
      },
    });
  });
};
