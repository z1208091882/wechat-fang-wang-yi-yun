import request from "../../../util/requset";
import Pubsub from "pubsub-js";
// pages/recommendSong/recommendSong.js
Page({
  /**
   * 页面的初始数据
   */
  data: {
    day: "", //天
    month: "", //月
    recommendList: [], //推荐每日列表的数据
    index: 0, //点击音乐的下标
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    // 判断用户是否登陆
    let userInfo = wx.getStorageSync("userInfo");
    if (!userInfo) {
      wx.showToast({
        title: "请登陆",
        icon: "none",
        success: () => {
          // 提示完成后跳转到登陆界面
          wx.reLaunch({
            url: "/pages/login/login",
          });
        },
      });
    }
    // 更新日期的状态数据
    this.setData({
      day: new Date().getDate(),
      month: new Date().getMonth() + 1,
    });

    // 获取每日推荐数据
    this.recommendList();

    // 订阅来自songDetail页面发布的消息
    //第一个是发布的名称，第二个参数是回调函数(可以接收两个参数，发布的名称和发布的内容)
    Pubsub.subscribe("switchType", (mas, type) => {
      // console.log(mas, type);
      let { recommendList, index } = this.data;
      if (type === "pre") {
        //上一首
        index > 0 ? (index -= 1) : recommendList.length - 1;
        // index = 0 && index === recommendList.length;
        // index -= 1;
      } else if (type === "next") {
        //下一首
        index === recommendList.length - 1 && (index = -1);
        index += 1;
      }
      // 更新下标
      this.setData({
        index,
      });
      let musicId = recommendList[index].id; //得到对应的上一首/下一首的id
      // 将我们得到的音乐的id回传给songdetail页面
      Pubsub.publish("musicId", musicId);
    });
  },
  // 获取每日推荐数据
  async recommendList() {
    let recommendListData = await request("recommend/songs");
    console.log(recommendListData);
    this.setData({
      recommendList: recommendListData.recommend,
    });
  },
  // 点击进行页面跳转到视频播放页面
  toSongDetail(event) {
    console.log(event);
    let songId = event.currentTarget.dataset.song.id; //获取传过来的对应的歌曲对象
    let index = event.currentTarget.dataset.index; //获取当前歌曲的下标
    // console.log(index);
    this.setData({
      index,
    });
    // console.log(songId);
    // 路由跳转传参 query方式
    wx.navigateTo({
      // 如果头转之前转为字符串 系统就不会在自动转为字符串了  不可以直接将song作为query参数传过去，因为长度过长，会被自动截取掉
      // url: "/pages/songDetail/songDetail?song=" + JSON.stringify(song),
      // 只需要将对应的id传过去就行了
      url: "/songPackage/pages/songDetail/songDetail?songId=" + songId,
    });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {},

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {},

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {},

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {},

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {},
});
