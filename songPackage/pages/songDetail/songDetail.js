import request from "../../../util/requset";
import Pubsub from "pubsub-js"; //引入的全局事件总线
import moment from "moment"; //引入时间转换格式
// 获取全局定义的实例
const appInstaance = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    isPlay: false, //音乐是否播放
    song: {}, //歌曲详情对象
    musicId: "", //音乐Id
    musicLink: "", //音乐的链接
    currentTime: "00:00", //实时时长
    durationTime: "00:00", //总时长
    currentWidth: 0, //实时音乐条的宽度
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    // 可以接收传过来的query数据
    // 原生小程序路由传参,对参数的长度有限制，如果参数的长度过长会自动截取掉
    // console.log(options);

    let musicId = options.songId; //得到传过来的当前歌曲对应的id
    // console.log(musicId);
    this.setData({
      musicId,
    });
    // 调用请求的音乐详情页的回调函数
    this.getMusicInfo(musicId);

    /*
    问题：如果用户操作系统的控制音乐播放/暂停的按钮，页面不知道，导致页面显示的是否播放的状态和真实的音乐播放的状态不一致

    解决方案：
      1.通过控制音频的backgroundAudioManager实例去监视音乐的播放/暂停（因为只能放在onLoad中因为只有这个是执行一次的）

    将这个实例提升到组件实例身上，这样在哪都可以调用了
    */

    // 判读当前音乐是否正在播放和在此进入的音乐和之前正在播放的音乐是不是同一个
    if (
      appInstaance.globalData.isMusicPlay &&
      appInstaance.globalData.musicId === musicId
    ) {
      // 如果是通一个就修改当前音乐的播放状态为true
      this.setData({ isPlay: true });
    }

    this.backgroundAudioManager = wx.getBackgroundAudioManager(); //获取全局唯一的背景音频管理器  小程序切入后台，如果音频处于播放状态，可以继续播放 必须将这个requiredBackgroundModes在app.json中进行设置
    // 监听音乐的播放/暂停/停止
    this.backgroundAudioManager.onPlay(() => {
      // 修改音乐播放时的状态
      this.changePlayState(true);

      // 修改全局音乐播放的状态
      // appInstaance.globalData.isMusicPlay = true;
      appInstaance.globalData.musicId = musicId;
    });
    this.backgroundAudioManager.onPause(() => {
      // 修改音乐暂停时的状态
      this.changePlayState(false);
      // 修改全局音乐播放的状态
      // appInstaance.globalData.isMusicPlay = false;
      // 因为在播放的时候已经修改了当前音乐的id 所以不用在此需要当前播放音乐的id
    });
    // 在真机模拟中，切到小程序切到后台时，有边栏有一个可以关闭小程序的X号
    this.backgroundAudioManager.onStop(() => {
      // 修改音乐停止时的状态
      this.changePlayState(false);
      // 修改全局音乐播放的状态
      // appInstaance.globalData.isMusicPlay = false;
    });

    // 监听背景音乐实时播放的进度
    this.backgroundAudioManager.onTimeUpdate(() => {
      // console.log("总时长", this.backgroundAudioManager.duration);
      // console.log("实时的时长", this.backgroundAudioManager.currentTime);
      // 格式化实时的播放时间 这个是秒转为毫秒
      let currentTime = moment(
        this.backgroundAudioManager.currentTime * 1000
      ).format("mm:ss");
      // 实时的进度条=实时的时间/视频的总长度*进度条的有限距离是450
      let currentWidth =
        (this.backgroundAudioManager.currentTime /
          this.backgroundAudioManager.duration) *
        450;
      // console.log(currentTime);
      // // 更新状态
      this.setData({
        currentTime,
        currentWidth,
      });
    });
    // 监听音乐自然结束
    this.backgroundAudioManager.onEnded(() => {
      // 自动切换为下一首音乐,并且自动播放
      Pubsub.publish("switchType", "next");
      // 并且将实时的进度条的长度还原成0，实时播放的时间也还原成0
      this.setData({
        currentWidth: 0,
        currentTime: "00:00", //起始时间
      });
    });
  },
  // 修改音乐播放状态的功能函数
  changePlayState(isPlay) {
    this.setData({
      isPlay,
    });
    // 修改全局音乐播放的状态
    appInstaance.globalData.isMusicPlay = isPlay;
  },
  // 获取音乐详情页的功能函数
  async getMusicInfo(musicId) {
    const result = await request("song/detail", {
      ids: musicId,
    });
    // result.song[0].dt//播放的总时长 单位是毫秒
    let durationTime = moment(result.songs[0].dt).format("mm:ss");
    // console.log(result);
    // 更新状态
    this.setData({
      song: result.songs[0],
      durationTime,
    });

    // 动态修改窗口的标题
    wx.setNavigationBarTitle({
      title: this.data.song.name, //因为setData是同步的行为，这样也可以直接拿到数据
    });
  },
  //点击播放或暂停的回调函数
  handleMusicPlay() {
    let isPlay = !this.data.isPlay;
    let { musicId, musicLink } = this.data;
    // this.setData({//在这里的修改状态也就没有用了
    //   // 修改是否播放的状态
    //   isPlay,
    // });
    this.musicControl(isPlay, musicId, musicLink);
  },
  // 控制播放/暂停的功能函数
  async musicControl(isPlay, musicId, musicLink) {
    //音乐播放
    if (isPlay) {
      if (!musicLink) {
        // 获取音乐播放链接
        let musicLinkData = await request("song/url", {
          id: musicId,
        });
        musicLink = musicLinkData.data[0].url;
        // 将得到的播放的链接存到状态里面
        this.setData({
          musicLink,
        });
      }

      // 创建控制音乐播放的实例对象
      this.backgroundAudioManager.src = musicLink; //播放的链接
      this.backgroundAudioManager.title = this.data.song.name; //播放的歌曲名称(必填不写没发播放)
    } else {
      //音乐暂停
      this.backgroundAudioManager.pause();
    }
  },
  // 点击切换上一首/下一首的回调函数
  handleSwitch(event) {
    // 获取切歌的类型
    let type = event.currentTarget.id; //传过来的上一首/下一首的数据的id值是不一样的

    // 关闭当前播放的音乐
    this.backgroundAudioManager.stop();

    // 订阅来自recommendSong页面发布的数据 订阅的音乐id一定在发送消息之前
    Pubsub.subscribe("musicId", (mas, musicId) => {
      console.log(mas, musicId);
      // 调用之前的请求函数获取最新的音乐详情的信息
      this.getMusicInfo(musicId);

      // 切完歌之后要进行自动播放音乐 调用之前的自动播放的函数
      this.musicControl(true, musicId);
      // 如果不取消订阅，每次点击完切歌时都会往数组进行累加，会被一起被调用所以会执行多次  所以要进行取消订阅 订阅一个 删除一个
      // 取消订阅
      Pubsub.unsubscribe("musicId");
    });

    // 发布消息数据给recommendSong页面 相对于emit
    Pubsub.publish("switchType", type);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {},

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {},

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {},

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {},

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {},
});
