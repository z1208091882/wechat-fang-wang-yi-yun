// components/NavHeader/NavHeader.js
Component({
  /**
   * 组件的属性列表,有组件外部传入数据，等同于vue中的props
   */
  properties: {
    /*
    type 是 单一属性的类型
    optionalTypes Array 否 属性的类型（ 可以指定多个） 2.6 .5
    value 否 属性的初始值   默认值
    observer Function 否 属性值变化时的回调函数 在vue中相当于watch
    */
    title: {
      type: String,
      value: "我是默认值"
    },
    nav: {
      type: String,
      value: "我是默认值nav"
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})